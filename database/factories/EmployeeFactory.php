<?php

use Faker\Generator as Faker;

$factory->define(App\Employee::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'phone' => $faker->phoneNumber,
        'email' => $faker->safeEmail,
        'address' => $faker->address,
        'role' => $faker->randomElement(['manager', 'normal']),
        'department_id' => function () {
            return factory(App\Department::class)->create();
        }
    ];
});
