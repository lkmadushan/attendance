<?php

use Faker\Generator as Faker;

$factory->define(App\Clocking::class, function (Faker $faker) {
    return [
        'type' => 'in',
        'created_at' => Carbon\Carbon::now(),
        'employee_id' => function() {
            return factory(App\Employee::class)->create();
        }
    ];
});
