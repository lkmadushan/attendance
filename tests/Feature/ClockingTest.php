<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ClockingTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function an_employee_can_clock_in()
    {
        $clocking = factory('App\Clocking')->make();

        $response = $this->postJson('/api/clockings', [
            'employee_id' => $clocking->employee_id
        ]);

        $response->assertStatus(200);
        $this->assertDatabaseHas('clockings', [
            'type' => 'in',
            'employee_id' => $clocking->employee_id,
            'created_at' => $clocking->created_at
        ]);
    }

    /**
     * @test
     */
    public function an_employee_can_clock_out()
    {
        $clocking = factory('App\Clocking')->make();

        $response = $this->deleteJson('/api/clockings', [
            'employee_id' => $clocking->employee_id
        ]);

        $response->assertStatus(200);
        $this->assertDatabaseHas('clockings', [
            'type' => 'out',
            'employee_id' => $clocking->employee_id,
            'created_at' => $clocking->created_at
        ]);
    }
}
