<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateDepartmentsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_required_a_name()
    {
        $this->post('/departments', [])
            ->assertSessionHasErrors('name');
    }

    /**
     * @test
     */
    public function it_can_create_a_department()
    {
        $department = factory('App\Department')->make();

        $response = $this->post('/departments', $department->toArray());

        $response->assertStatus(200);
        $this->assertDatabaseHas('departments', $department->toArray());
    }

    /**
     * @test
     */
    public function it_can_delete_a_department()
    {
        $departments = factory('App\Department', 5)->create();
        $departmentToDelete = $departments->first();

        $response = $this->delete('/departments/'.$departmentToDelete->id);

        $response->assertStatus(204);
        $this->assertDatabaseMissing('departments', $departmentToDelete->toArray());
    }

    /**
     * @test
     */
    public function it_can_update()
    {
        $department = factory('App\Department')->create();
        $updatedDepartment = factory('App\Department')->make();

        $response = $this->patch('/departments/'.$department->id, $updatedDepartment->toArray());

        $response->assertStatus(200);
        $this->assertDatabaseHas('departments', $updatedDepartment->toArray());
    }
}
