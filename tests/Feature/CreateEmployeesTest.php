<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateEmployeesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_required_a_name()
    {
        $this->post('/employees', [])
            ->assertSessionHasErrors('name');
    }

    /**
     * @test
     */
    public function it_required_a_unique_email()
    {
        $employee = factory('App\Employee')->create([
            'email' => 'foo@bar.com'
        ]);

        $this->post('/employees', [
            'name' => $employee->name,
            'email' => 'foo@bar.com'
        ])->assertSessionHasErrors('email');

        $this->post('/employees', [
            'name' => $employee->name,
            'email' => 'foo'
        ])->assertSessionHasErrors('email');
    }

    /**
     * @test
     */
    public function it_can_create_an_employee()
    {
        $employee = factory('App\Employee')->make();

        $response = $this->post('/employees', $employee->toArray());

        $response->assertStatus(200);
        $this->assertDatabaseHas('employees', $employee->toArray());
    }

    /**
     * @test
     */
    public function it_can_delete_a_department()
    {
        $employee = factory('App\Employee', 5)->create();
        $employeeToDelete = $employee->first();

        $response = $this->delete('/employees/'.$employeeToDelete->id);

        $response->assertStatus(204);
        $this->assertDatabaseMissing('employees', $employeeToDelete->toArray());
    }

    /**
     * @test
     */
    public function it_can_update()
    {
        $employee = factory('App\Employee')->create();
        $updatedEmployee = factory('App\Employee')->make();

        $response = $this->patch('/employees/'.$employee->id, $updatedEmployee->toArray());

        $response->assertStatus(200);
        $this->assertDatabaseHas('employees', $updatedEmployee->toArray());
    }
}
