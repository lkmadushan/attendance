<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_knows_department()
    {
        $employee = factory('App\Employee')->create();

        $this->assertInstanceOf('App\Department', $employee->department);
    }
}
