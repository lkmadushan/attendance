<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clocking extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Set in/out time of the employee
     *
     * @void
     */
    public function setCreatedAtAttribute()
    {
        $this->setCreatedAt($this->freshTimestamp());
    }

    /**
     * Latest clocking status query of the employee
     *
     * @param $query
     * @param $employeeId
     * @return mixed
     */
    public function scopeLatestForEmployee($query, $employeeId)
    {
        return $query->where('employee_id', $employeeId)->latest();
    }

    /**
     * Relation with employee
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
