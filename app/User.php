<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Employee relation with user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    /**
     * Determine employee's role is admin
     *
     * @return bool
     */
    public function isAdmin()
    {
        return is_null($this->employee);
    }

    /**
     * Determine employee's role is manager
     *
     * @return bool
     */
    public function isManager()
    {
        if ($this->employee) {
            return 'manager' == $this->employee->role;
        }
    }

    /**
     * Determine employee's role is normal
     *
     * @return bool
     */
    public function isNormal()
    {
        if ($this->employee) {
            return 'normal' == $this->employee->role;
        }
    }

    /**
     * User's display name
     *
     * @return mixed
     */
    public function displayName()
    {
        if ($this->employee) {
            return $this->employee->name;
        }

        return $this->name;
    }
}
