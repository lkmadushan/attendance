<?php

namespace App\Http\Controllers;

use App\Department;

class DepartmentsController extends Controller
{
    /**
     * List all departments
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::all();

        return view('departments.list', compact('departments'));
    }

    /**
     * Create a department
     *
     * @return Department
     */
    public function store()
    {
        request()->validate([
            'name' => 'required'
        ]);

        $department = Department::create([
            'name' => request('name'),
            'description' => request('description')
        ]);

        return $department;
    }

    /**
     * Update a department
     *
     * @param Department $department
     * @return Department
     */
    public function update(Department $department)
    {
        request()->validate([
            'name' => 'required'
        ]);

        $department->update([
            'name' => request('name'),
            'description' => request('description')
        ]);

        return $department;
    }

    /**
     * Delete a department
     *
     * @param Department $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        $department->delete();

        return response([], 204);
    }
}
