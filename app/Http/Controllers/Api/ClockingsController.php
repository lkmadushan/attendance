<?php

namespace App\Http\Controllers\Api;

use App\Clocking;
use App\Http\Controllers\Controller;

class ClockingsController extends Controller
{
    /**
     * List all employee's clocking stream
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        $employee = request('employee_id');

        if ($employee) {
            return Clocking::with('employee')
                ->whereEmployeeId($employee)
                ->latest()
                ->get();
        }

        return Clocking::with('employee')
            ->latest()
            ->get();
    }

    /**
     * Set an employee's time in
     *
     * @return Clocking
     */
    public function store()
    {
        request()->validate([
            'employee_id' => 'exists:employees,id'
        ]);

        $clocking = Clocking::create([
            'type' => 'in',
            'employee_id' => request('employee_id')
        ]);

        return $clocking->load('employee');
    }

    /**
     * Set an employee's time out
     *
     * @return Clocking
     */
    public function destroy()
    {
        request()->validate([
            'employee_id' => 'exists:employees,id'
        ]);

        $clocking = Clocking::create([
            'type' => 'out',
            'employee_id' => request('employee_id')
        ]);

        return $clocking->load('employee');
    }
}
