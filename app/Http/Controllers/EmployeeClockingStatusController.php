<?php

namespace App\Http\Controllers;

use App\Clocking;

class EmployeeClockingStatusController extends Controller
{
    /**
     * Employee's latest clocking status
     *
     * @return mixed
     */
    public function index()
    {
        request()->validate([
            'employee_id' => 'exists:employees,id'
        ]);

        return Clocking::latestForEmployee(request('employee_id'))->first();
    }
}