<?php

namespace App\Http\Controllers;

use App\Roster;

class RostersController extends Controller
{
    /**
     * List all rosters.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $rosters = Roster::all();

        return view('rosters.list', compact('rosters'));
    }

    /**
     * Store a new roster.
     *
     * @return Roster
     */
    public function store()
    {
        request()->validate([
            'name' => 'required',
            'description' => 'required',
            'schedule' => 'required'
        ]);

        $roster = Roster::create([
            'name' => request('name'),
            'description' => request('description'),
            'schedule' => request('schedule')
        ]);

        return $roster;
    }

    /**
     * Update the roster.
     *
     * @param Roster $roster
     * @return Roster
     */
    public function update(Roster $roster)
    {
        request()->validate([
            'name' => 'required',
            'description' => 'required',
            'schedule' => 'required'
        ]);

        $roster->update([
            'name' => request('name'),
            'description' => request('description'),
            'schedule' => request('schedule')
        ]);

        return $roster;
    }

    /**
     * Remove roster from storage.
     *
     * @param Roster $roster
     * @return mixed
     */
    public function destroy(Roster $roster)
    {
        $roster->delete();

        return response([], 204);
    }
}
