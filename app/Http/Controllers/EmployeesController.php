<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Department;
use App\Roster;
use Illuminate\Validation\Rule;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::all();
        $rosters = Roster::all();
        $employees = Employee::with('department')->get();

        return view('employees.list',
            compact('employees', 'departments', 'rosters')
        );
    }

    /**
     * Store the resource.
     *
     * @return Employee
     */
    public function store()
    {
        request()->validate([
            'name' => 'required',
            'email' => 'nullable|email|unique:employees'
        ]);

        $employee = Employee::create([
            'name' => request('name'),
            'email' => request('email'),
            'phone' => request('phone'),
            'address' => request('address'),
            'department_id' => request('department_id'),
            'roster_id' => request('roster_id'),
            'role' => request('role'),
        ]);

        return $employee->load('department');
    }

    /**
     * Update the resource
     *
     * @param Employee $employee
     * @return Employee
     */
    public function update(Employee $employee)
    {
        request()->validate([
            'name' => 'required',
            'email' => [
                'nullable', 'email',
                Rule::unique('employees')->ignore($employee->id)
            ]
        ]);

        $employee->update([
            'name' => request('name'),
            'email' => request('email'),
            'phone' => request('phone'),
            'address' => request('address'),
            'department_id' => request('department_id'),
            'roster_id' => request('roster_id'),
            'role' => request('role'),
        ]);

        return $employee->load('department');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();

        return response([], 204);
    }
}
