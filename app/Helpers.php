<?php

/**
 * Set active class by path
 *
 * @param $path
 * @param string $class
 * @return string
 */
function active_class($path, $class = 'active')
{
    if (request()->is($path.'*')) {
        return $class;
    }
}

/**
 * Generate breadcrumb title
 *
 * @param $page
 * @return string
 */
function breadcrumb_title($page)
{
    return ucwords(str_replace('_', ' ', $page));
}

/**
 * Set breadcrumb class by path
 *
 * @param $path
 * @param $page
 * @param string $class
 * @return string
 */
function active_breadcrumb_class($path, $page, $class = 'active')
{
    if ($path == explode('_', $page)[0]) {
        return $class;
    }
}
