<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::group(['middleware' => 'auth'], function () {
    Route::view('/', 'home.index')->name("home");

    Route::view('/dayoffs', 'dayoffs.list')->name("dayoffs");
    Route::view('/overtimes', 'overtimes.list')->name("overtimes");
    Route::view('/assesments', 'assesments.list')->name("assesments");
    Route::view('/timesheets', 'timesheets.list')->name("timesheets");

    Route::get('/employees', 'EmployeesController@index')->name("employees");
    Route::post('/employees', 'EmployeesController@store');
    Route::patch('/employees/{employee}', 'EmployeesController@update');
    Route::delete('/employees/{employee}', 'EmployeesController@destroy');

    Route::get('/departments', 'DepartmentsController@index')->name("departments");
    Route::post('/departments', 'DepartmentsController@store');
    Route::patch('/departments/{department}', 'DepartmentsController@update');
    Route::delete('/departments/{department}', 'DepartmentsController@destroy');

    Route::get('/rosters', 'RostersController@index')->name("rosters");
    Route::post('/rosters', 'RostersController@store');
    Route::patch('/rosters/{roster}', 'RostersController@update');
    Route::delete('/rosters/{roster}', 'RostersController@destroy');

    Route::get('/clocking-status', 'EmployeeClockingStatusController@index')->name("clocking-status");
});
