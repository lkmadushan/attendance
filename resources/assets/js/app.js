
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

window.events = new Vue();

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('departments-view', require('./views/Departments.vue'));
Vue.component('employees-view', require('./views/Employees.vue'));
Vue.component('rosters-view', require('./views/Rosters.vue'));
Vue.component('clocking', require('./components/Clocking.vue'));
Vue.component('clocking-stream', require('./components/ClockingStream.vue'));

const app = new Vue({
    el: '#app',

    mounted() {
        $('.footable').footable();
    }
});
