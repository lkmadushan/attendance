export default {
    methods: {
        /**
         * Reset form data
         */
        resetForm() {
            let form = this.form;

            _.forOwn(form, (value, key) => {
                form[key] = ''
            });
        }
    }
}