export default {
    methods: {
        /**
         * Hide modal window
         *
         * @param modal
         */
        hideModal(modal) {
            $(`#${modal}`).modal('hide');
        },

        /**
         * Show modal window
         *
         * @param modal
         */
        showModal(modal) {
            $(`#${modal}`).modal();
        }
    }
}