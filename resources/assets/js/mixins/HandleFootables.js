export default {
    methods: {
        /**
         * Initialize footable
         *
         * @param table
         */
        initializeTable(table) {
            $(`#${table}`).footable({
                pageSize: 15,
                limitNavigation: 5
            });
        },

        /**
         * Redraw footable
         *
         * @param table
         */
        redrawTable(table) {
            Vue.nextTick(() => {
                $(`#${table}`).trigger('footable_redraw');
            });
        }
    }
}