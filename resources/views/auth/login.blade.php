<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }} | Login Page</title>

    <link rel="stylesheet" href="{!! asset('css/app.css') !!}"/>
</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name">At+</h1>
        </div>
        <h3>Welcome to At+</h3>
        <p>Perfectly designed and precisely prepared attendance management web application.
        </p>
        <p>Login in. To see it in action.</p>
        <form class="m-t" action="{{ route('login') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <input type="email"
                       class="form-control"
                       placeholder="Email"
                       name="email"
                       required>
            </div>
            <div class="form-group">
                <input type="password"
                       class="form-control"
                       placeholder="Password"
                       name="password"
                       required>
            </div>
            <button class="btn btn-primary block full-width m-b">Login</button>
            <a href="#"><small>Forgot password?</small></a>
        </form>
        <p class="m-t"> <small>At+ web application &copy; 2014</small> </p>
    </div>
</div>

<script src="{!! asset('js/manifest.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/vendor.js') !!}" type="text/javascript"></script>
</body>
</html>
