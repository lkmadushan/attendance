@extends('layouts.app')

@section('title', 'Day Offs')

@section('content')
    @include('layouts.partials._breadcrumb', ['paths' => ['dayoffs'], 'page' => 'dayoffs'])

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox">
                    <a class="collapse-link">
                        <div class="ibox-title">
                            <h5>List</h5>
                            <div class="ibox-tools">
                                <i class="fa fa-chevron-up"></i>
                            </div>
                        </div>
                    </a>
                    <div class="ibox-content">
                        <table class="table footable table-stripped toggle-arrow-tiny">
                            <thead>
                            <tr>
                                <th data-hide="phone">Start</th>
                                <th data-hide="phone">End</th>
                                <th data-hide="phone">Duration</th>
                                <th data-hide="phone">Approved By</th>
                                <th data-hide="phone">Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>2017-01-01 11:30 AM</td>
                                <td>2017-01-01 12:30 PM</td>
                                <td>60 minutes</td>
                                <td>Russell Rowe III</td>
                                <td><span class="label label-danger">Discard</span></td>
                            </tr>
                            <tr>
                                <td>2017-01-01 11:30 AM</td>
                                <td>2017-01-01 12:35 PM</td>
                                <td>65 minutes</td>
                                <td>Willow Krajcik</td>
                                <td><span class="label label-info">Pending</span></td>
                            </tr>
                            <tr>
                                <td>2017-01-01 10:30 AM</td>
                                <td>2017-01-01 12:30 PM</td>
                                <td>120 minutes</td>
                                <td>Willow Krajcik</td>
                                <td><span class="label label-primary">Approved</span></td>
                            </tr>
                            <tr>
                                <td>2017-01-01 11:30 AM</td>
                                <td>2017-01-01 12:40 PM</td>
                                <td>70 minutes</td>
                                <td>Russell Rowe III</td>
                                <td><span class="label label-danger">Discard</span></td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="5">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
