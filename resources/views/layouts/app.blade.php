<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', config('app.name'))</title>

    <link rel="stylesheet" href="{!! asset('css/app.css') !!}"/>

    <script>
        window.App = {!! json_encode([
            'csrfToken' => csrf_token(),
            'user' => auth()->user(),
            'signedIn' => auth()->check()
        ]) !!};
    </script>
</head>

<body>

<div id="app">
    <div id="wrapper">
        @include('layouts.partials._navigation-side')

        <div id="page-wrapper" class="gray-bg">
            @include('layouts.partials._navigation-top')

            @yield('content')

            @include('layouts.partials._footer')
        </div>
    </div>
</div>

<script src="{!! asset('js/manifest.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/vendor.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/app.js') !!}" type="text/javascript"></script>
</body>
</html>
