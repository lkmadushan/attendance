<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>{{ breadcrumb_title($page) }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('home') }}">Home</a>
            </li>
            @foreach($paths as $path)
            <li class="{{ active_breadcrumb_class($path, $page) }}">
                @if(active_breadcrumb_class($path, $page))
                    <strong>{{ ucfirst($path) }}</strong>
                @else
                    <a href="{{ route($path) }}">{{ ucfirst($path) }}</a>
                @endif
            </li>
            @endforeach
        </ol>
    </div>
</div>