<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <img alt="image" class="img-circle" src="{{ asset('profile_small.jpg') }}"/>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">{{ auth()->user()->displayName() }}</strong>
                            </span> <span class="text-muted text-xs block">Profile <b class="caret"></b></span>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="#">Change Password</a></li>
                        <li><a href="{{ route('logout') }}">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">At+</div>
            </li>
            @if(auth()->user()->isManager() || auth()->user()->isNormal())
            <li class="{{ active_class('/') }}">
                <a href="{{ route('home') }}">
                    <i class="fa fa-th-large"></i>
                    <span class="nav-label">Dashboard</span>
                </a>
            </li>
            <li class="{{ active_class('overtimes') }}">
                <a href="{{ route('overtimes') }}">
                    <i class="fa fa-clock-o"></i>
                    <span class="nav-label">Overtime</span>
                </a>
            </li>
            <li class="{{ active_class('dayoffs') }}">
                <a href="{{ route('dayoffs') }}">
                    <i class="fa fa-calendar-times-o"></i>
                    <span class="nav-label">Day Offs</span>
                </a>
            </li>
            @endif

            @if(auth()->user()->isManager())
                <li class="{{ active_class('assesments') }}">
                    <a href="{{ route('assesments') }}">
                        <i class="fa fa-check-square-o"></i>
                        <span class="nav-label">Assesments</span>
                    </a>
                </li>
                <li class="{{ active_class('timesheets') }}">
                    <a href="{{ route('timesheets') }}">
                        <i class="fa fa-calendar-check-o"></i>
                        <span class="nav-label">Timesheets</span>
                    </a>
                </li>
            @endif
            @if(auth()->user()->isAdmin())
                <li class="{{ active_class('departments') }}">
                    <a href="{{ route('departments') }}">
                        <i class="fa fa-building-o"></i>
                        <span class="nav-label">Departments</span>
                    </a>
                </li>
                <li class="{{ active_class('employees') }}">
                    <a href="{{ route('employees') }}">
                        <i class="fa fa-group"></i>
                        <span class="nav-label">Employees </span>
                    </a>
                </li>
                <li class="{{ active_class('rosters') }}">
                    <a href="{{ route('rosters') }}">
                        <i class="fa fa-history"></i>
                        <span class="nav-label">Rosters</span>
                    </a>
                </li>
                <li class="#">
                    <a href="#">
                        <i class="fa fa-key"></i>
                        <span class="nav-label">Permissions</span>
                    </a>
                </li>
                <li class="#">
                    <a href="#">
                        <i class="fa fa-cog"></i>
                        <span class="nav-label">Settings</span>
                    </a>
                </li>
            @endif
        </ul>
    </div>
</nav>
