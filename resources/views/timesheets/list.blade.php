@extends('layouts.app')

@section('title', 'Timesheets')

@section('content')
    @include('layouts.partials._breadcrumb', ['paths' => ['timesheets'], 'page' => 'timesheets'])

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox">
                    <a class="collapse-link">
                        <div class="ibox-title">
                            <h5>List</h5>
                            <div class="ibox-tools">
                                <i class="fa fa-chevron-up"></i>
                            </div>
                        </div>
                    </a>
                    <div class="ibox-content">
                        <table class="table footable table-stripped toggle-arrow-tiny">
                            <thead>
                            <tr>
                                <th data-toggle="true">Employee</th>
                                <th data-hide="phone">Clock In</th>
                                <th data-hide="phone">Clock Out</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Alexandra Donnelly</td>
                                <td>2017-01-01 10:30 AM</td>
                                <td>2017-01-01 12:30 PM</td>
                            </tr>
                            <tr>
                                <td>Elliot Denesik</td>
                                <td>2017-01-01 09:30 AM</td>
                                <td>2017-01-01 10:30 AM</td>
                            </tr>
                            <tr>
                                <td>Caesar Cronin</td>
                                <td>2017-01-01 10:30 AM</td>
                                <td>2017-01-01 11.30 AM</td>
                            </tr>
                            <tr>
                                <td>Roxane Murazik</td>
                                <td>2017-01-01 10:30 AM</td>
                                <td>2017-01-03 10:30 AM</td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="3">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
