@extends('layouts.app')

@section('title', 'Departments')

@section('content')
    @include('layouts.partials._breadcrumb', ['paths' => ['departments'], 'page' => 'departments'])

    <departments-view :departments="{{ $departments }}" inline-template v-cloak>
        <div class="wrapper wrapper-content">
            <button type="button" class="btn btn-primary pull-right m-b-md"
                    @click="createDepartment">
                <i class="fa fa-building-o"></i> &nbsp;Create department
            </button>
            <div class="modal inmodal animated fadeIn"
                 id="departments-create"
                 tabindex="-1"
                 role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="ibox">
                                <div class="ibox-title">
                                    <h5>Create Department</h5>
                                    <div class="ibox-tools">
                                        <a data-dismiss="modal">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <form @submit.prevent="saveDepartment">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label"
                                                           for="name">Name</label>
                                                    <input v-model="form.name"
                                                           placeholder="Name"
                                                           class="form-control"
                                                           required>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label"
                                                           for="description">Description</label>
                                                    <input v-model="form.description"
                                                           placeholder="Description"
                                                           class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <button type="button"
                                                    class="btn btn-white"
                                                    @click="resetForm"
                                                    data-dismiss="modal">
                                                <i class="fa fa-close"></i> Close
                                            </button>
                                            <button class="btn btn-primary">
                                                <i class="fa fa-floppy-o"></i> Save changes
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="ibox">
                        <a class="collapse-link">
                            <div class="ibox-title">
                                <h5>List</h5>
                                <div class="ibox-tools">
                                    <i class="fa fa-chevron-up"></i>
                                </div>
                            </div>
                        </a>
                        <div class="ibox-content">
                            <table id="departments-table"
                                   class="table table-stripped toggle-arrow-tiny">
                                <thead>
                                <tr>
                                    <th data-toggle="true">Name</th>
                                    <th data-hide="phone">Description</th>
                                    <th class="text-right" data-sort-ignore="true">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="department in departments" v-cloak>
                                    <td v-text="department.name"></td>
                                    <td v-text="department.description"></td>
                                    <td class="text-right">
                                        <button @click="editDepartment(department)" class="btn-white btn btn-xs">Edit</button>
                                        <button @click="removeDepartment(department)" class="btn-white btn btn-xs">Delete</button>
                                    </td>
                                </tr>
                                <tr v-if="!departments.length" class="text-center">
                                    <td colspan="3">No results</td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr v-if="departments.length">
                                    <td colspan="3">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </departments-view>
@endsection
