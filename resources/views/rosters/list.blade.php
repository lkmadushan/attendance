@extends('layouts.app')

@section('title', 'Rosters')

@section('content')
    @include('layouts.partials._breadcrumb', ['paths' => ['rosters'], 'page' => 'rosters'])

    <rosters-view :rosters="{{ $rosters }}"
                    inline-template
                    v-cloak>
        <div class="wrapper wrapper-content">
            <button type="button" class="btn btn-primary pull-right m-b-md"
                    @click="createRoster">
                <i class="fa fa-user-o"></i> &nbsp;Create roster
            </button>
            <div class="modal inmodal animated fadeIn"
                 id="rosters-create"
                 tabindex="-1"
                 role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="ibox">
                                <div class="ibox-title">
                                    <h5>Create Roster</h5>
                                    <div class="ibox-tools">
                                        <a data-dismiss="modal">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <form @submit.prevent="saveRoster">
                                    <div class="ibox-content">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label"
                                                           for="name">Name</label>
                                                    <input v-model="form.name"
                                                           placeholder="Name"
                                                           class="form-control"
                                                           required>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label"
                                                           for="description">Description</label>
                                                    <input v-model="form.description"
                                                           placeholder="Description"
                                                           class="form-control"
                                                           required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ibox-content">
                                        <div class="row" v-for="item in schedule">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label"
                                                           for="from">From</label>
                                                    <input name="from"
                                                           type="time"
                                                           placeholder="From"
                                                           class="form-control"
                                                           v-model="item.from"
                                                           required>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label"
                                                           for="to">To</label>
                                                    <input name="to"
                                                           type="time"
                                                           placeholder="To"
                                                           class="form-control"
                                                           v-model="item.to"
                                                           required>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label"
                                                           for="type">Type</label>
                                                    <select class="form-control"
                                                            name="type"
                                                            v-model="item.type">
                                                        <option selected disabled value="">Select a type</option>
                                                        <option v-for="type in types"
                                                                v-text="type.name"
                                                                :value="type.value"></option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <button type="button"
                                                    class="btn btn-white"
                                                    @click="closeModel">
                                                <i class="fa fa-close"></i> Close
                                            </button>
                                            <button class="btn btn-danger"
                                                    @click.prevent="removeSchedule">
                                                <i class="fa fa-clock-o"></i> Remove
                                            </button>
                                            <button class="btn btn-info"
                                                    @click.prevent="addSchedule">
                                                <i class="fa fa-clock-o"></i> Add
                                            </button>
                                            <button class="btn btn-primary">
                                                <i class="fa fa-floppy-o"></i> Save changes
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="ibox">
                        <a class="collapse-link">
                            <div class="ibox-title">
                                <h5>List</h5>
                                <div class="ibox-tools">
                                    <i class="fa fa-chevron-up"></i>
                                </div>
                            </div>
                        </a>
                        <div class="ibox-content">
                            <table id="rosters-table"
                                   class="table table-stripped toggle-arrow-tiny">
                                <thead>
                                <tr>
                                    <th data-toggle="true">Name</th>
                                    <th data-toggle="true"
                                        data-hide="phone">
                                        Description
                                    </th>
                                    <th data-hide="all">Schedule</th>
                                    <th class="text-right" data-sort-ignore="true">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="roster in rosters" :key="roster.id">
                                    <td v-text="roster.name"></td>
                                    <td v-text="roster.description"></td>
                                    <td>
                                        <div v-for="schedule in roster.schedule">
                                            <p>Type : @{{ schedule.type }}</p>
                                            <p>From : @{{ schedule.from }}</p>
                                            <p>To : @{{ schedule.to }}</p>

                                            <hr>
                                        </div>
                                    </td>
                                    <td class="text-right">
                                        <button @click="editRoster(roster)" class="btn-white btn btn-xs">Edit</button>
                                        <button @click="removeRoster(roster)" class="btn-white btn btn-xs">Delete</button>
                                    </td>
                                </tr>
                                <tr v-if="!rosters.length" class="text-center">
                                    <td colspan="6">No results</td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr v-if="rosters.length">
                                    <td colspan="6">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </rosters-view>
@endsection
