@extends('layouts.app')

@section('title', 'Assesments')

@section('content')
    @include('layouts.partials._breadcrumb', ['paths' => ['assesments'], 'page' => 'assesments'])

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox">
                    <a class="collapse-link">
                        <div class="ibox-title">
                            <h5>List</h5>
                            <div class="ibox-tools">
                                <i class="fa fa-chevron-up"></i>
                            </div>
                        </div>
                    </a>
                    <div class="ibox-content">
                        <table class="table footable table-stripped toggle-arrow-tiny">
                            <thead>
                            <tr>
                                <th data-toggle="true">Employee</th>
                                <th data-hide="phone">Start</th>
                                <th data-hide="phone">End</th>
                                <th data-hide="phone">Duration</th>
                                <th data-hide="phone">Approved By</th>
                                <th data-hide="phone">Type</th>
                                <th class="text-right" data-sort-ignore="true">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Alexandra Donnelly</td>
                                <td>2017-01-01 10:30 AM</td>
                                <td>2017-01-01 12:30 PM</td>
                                <td>120 minutes</td>
                                <td>Russell Rowe III</td>
                                <td>Dayoff</td>
                                <td class="text-right">
                                    <button class="btn btn-xs btn-danger">Discard</button>
                                    <button class="btn btn-xs btn-info">Approve</button>
                                </td>
                            </tr>
                            <tr>
                                <td>Elliot Denesik</td>
                                <td>2017-01-01 09:30 AM</td>
                                <td>2017-01-01 10:30 AM</td>
                                <td>60 minutes</td>
                                <td>Willow Krajcik</td>
                                <td>Overtime</td>
                                <td class="text-right">
                                    <button class="btn btn-xs btn-danger">Discard</button>
                                    <button class="btn btn-xs btn-info">Approve</button>
                                </td>
                            </tr>
                            <tr>
                                <td>Caesar Cronin</td>
                                <td>2017-01-01 10:30 AM</td>
                                <td>2017-01-01 11.30 AM</td>
                                <td>60 minutes</td>
                                <td>Willow Krajcik</td>
                                <td>Overtime</td>
                                <td class="text-right">
                                    <button class="btn btn-xs btn-danger">Discard</button>
                                    <button class="btn btn-xs btn-info">Approve</button>
                                </td>
                            </tr>
                            <tr>
                                <td>Roxane Murazik</td>
                                <td>2017-01-01 10:30 AM</td>
                                <td>2017-01-03 10:30 AM</td>
                                <td>2 days</td>
                                <td>Russell Rowe III</td>
                                <td>Dayoff</td>
                                <td class="text-right">
                                    <button class="btn btn-xs btn-danger">Discard</button>
                                    <button class="btn btn-xs btn-info">Approve</button>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="7">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
