@extends('layouts.app')

@section('title', 'Main page')

@section('content')
    @include('layouts.partials._breadcrumb', ['paths' => ['dashboard'], 'page' => 'dashboard'])

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-7" v-cloak>
                <clocking-stream></clocking-stream>
            </div>
            <div class="col-lg-2">

            </div>
            <div class="col-lg-3" v-cloak>
                <clocking></clocking>
            </div>
        </div>
    </div>
@endsection
