@extends('layouts.app')

@section('title', 'Employees')

@section('content')
    @include('layouts.partials._breadcrumb', ['paths' => ['employees'], 'page' => 'employees'])

    <employees-view :employees="{{ $employees }}"
                    :departments="{{ $departments }}"
                    :rosters="{{ $rosters }}"
                    inline-template
                    v-cloak>
        <div class="wrapper wrapper-content">
            <button type="button" class="btn btn-primary pull-right m-b-md"
                    @click="createEmployee">
                <i class="fa fa-user-o"></i> &nbsp;Create emlpoyee
            </button>
            <div class="modal inmodal animated fadeIn"
                 id="employees-create"
                 tabindex="-1"
                 role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="ibox">
                                <div class="ibox-title">
                                    <h5>Create Employee</h5>
                                    <div class="ibox-tools">
                                        <a data-dismiss="modal">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <form @submit.prevent="saveEmployee">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label"
                                                           for="name">Name</label>
                                                    <input v-model="form.name"
                                                           placeholder="Name"
                                                           class="form-control"
                                                           required>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label"
                                                           for="phone">Phone</label>
                                                    <input v-model="form.phone"
                                                           placeholder="Phone"
                                                           class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label"
                                                           for="name">Email</label>
                                                    <input v-model="form.email"
                                                           placeholder="Email"
                                                           class="form-control"
                                                           type="email">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label"
                                                           for="address">Address</label>
                                                    <input v-model="form.address"
                                                           placeholder="Address"
                                                           class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label"
                                                           for="department_id">Department</label>
                                                    <select class="form-control"
                                                            name="department_id"
                                                            v-model="form.department_id">
                                                        <option selected disabled value="">Select a department</option>
                                                        <option v-for="department in departments"
                                                                v-text="department.name"
                                                                :value="department.id"></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label"
                                                           for="roaster_id">Roaster</label>
                                                    <select class="form-control"
                                                            name="roster_id"
                                                            v-model="form.roster_id">
                                                        <option selected disabled value="">Select a roster</option>
                                                        <option v-for="roster in rosters"
                                                                v-text="roster.name"
                                                                :value="roster.id"></option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label"
                                                           for="department_id">Role</label>
                                                    <select class="form-control"
                                                            name="role"
                                                            v-model="form.role">
                                                        <option selected disabled value="">Select a roster</option>
                                                        <option v-for="role in roles"
                                                                v-text="role.name"
                                                                :value="role.id"></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">

                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <button type="button"
                                                    class="btn btn-white"
                                                    @click="resetForm"
                                                    data-dismiss="modal">
                                                <i class="fa fa-close"></i> Close
                                            </button>
                                            <button class="btn btn-primary">
                                                <i class="fa fa-floppy-o"></i> Save changes
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="ibox">
                        <a class="collapse-link">
                            <div class="ibox-title">
                                <h5>List</h5>
                                <div class="ibox-tools">
                                    <i class="fa fa-chevron-up"></i>
                                </div>
                            </div>
                        </a>
                        <div class="ibox-content">
                            <table id="employees-table"
                                   class="table table-stripped toggle-arrow-tiny">
                                <thead>
                                <tr>
                                    <th data-toggle="true">Name</th>
                                    <th data-toggle="true"
                                        data-hide="phone">
                                        Department
                                    </th>
                                    <th data-hide="phone">Phone</th>
                                    <th data-hide="phone">Email</th>
                                    <th data-hide="phone">Address</th>
                                    <th class="text-right" data-sort-ignore="true">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="employee in employees" :key="employee.id">
                                    <td v-text="employee.name"></td>
                                    <td v-text="employee.department ? employee.department.name : ''"></td>
                                    <td v-text="employee.phone"></td>
                                    <td v-text="employee.email"></td>
                                    <td v-text="employee.address"></td>
                                    <td class="text-right">
                                        <button @click="editEmployee(employee)" class="btn-white btn btn-xs">Edit</button>
                                        <button @click="removeEmployee(employee)" class="btn-white btn btn-xs">Delete</button>
                                    </td>
                                </tr>
                                <tr v-if="!employees.length" class="text-center">
                                    <td colspan="6">No results</td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr v-if="employees.length">
                                    <td colspan="6">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </employees-view>
@endsection
