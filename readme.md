# Attendee

> Attendance Management System

## How to run

Make sure you have already installed [Docker](https://www.docker.com).

```bash
# Download Repository
git clone https://lkmadushan@bitbucket.org/lkmadushan/attendance.git
cd attendance

# Start Laravel application (You can access application through http://localhost)
docker-compose up -d
```
